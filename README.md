denkmal-gtk
===========

Experimental GTK client for Denkmal.org written in Rust.

Features:
- Load events data from the GraphQL API.
- GTK frontend based on [gtk-rs](https://gtk-rs.org/).
- Actor-based multi-threading (GTK-mainloop runs in the main thread, a futures-threadpool runs the "backend" actor).

![](docs/screenshots/screenshot.png)

Usage
-----

Run the application:
```
cargo run
```

Development
-----------

### GraphQL

Re-generate the GraphQL Rust code (`src/graphql/queries.rs`):
```
cargo make graphql-generate
```
