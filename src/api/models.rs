use crate::graphql::get_events::*;

pub struct SparseData {
    pub regions: Vec<SparseDataRegion>,
}

pub struct SparseDataRegion {
    pub slug: String,
    pub days: Vec<SparseDataDay>,
}

pub struct SparseDataDay {
    pub events: Vec<GetEventsRegionsEvents>,
}

// Some API fields are nullable, but actually always set, so removing the `Option`s.
// See https://gitlab.com/denkmal/denkmal-api/issues/79
impl From<ResponseData> for SparseData {
    fn from(data: ResponseData) -> Self {
        let regions = data.regions
            .unwrap_or_default()
            .into_iter()
            .filter_map(|region_opt| {
                region_opt.map(|region| {
                    let days = region.events
                        .unwrap_or_default()
                        .into_iter()
                        .filter_map(|day_opt| {
                            day_opt.map(|day| {
                                let events = day
                                    .into_iter().filter_map(|e| e)
                                    .collect();
                                SparseDataDay {
                                    events,
                                }
                            })
                        })
                        .collect();
                    SparseDataRegion {
                        slug: region.slug,
                        days,
                    }
                })
            })
            .collect();
        SparseData {
            regions
        }
    }
}
