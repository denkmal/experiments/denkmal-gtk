use std::error::Error;

use graphql_client::{GraphQLQuery, Response};

use crate::api::models::*;
use crate::graphql::*;

pub async fn get_events() -> SparseData {
    let variables = get_events::Variables {
        event_days: vec!(
            EventDay::from("2020-02-07"),
            EventDay::from("2020-02-08"),
            EventDay::from("2020-02-09"),
            EventDay::from("2020-02-10"),
            EventDay::from("2020-02-11"),
            EventDay::from("2020-02-12"),
            EventDay::from("2020-02-13"),
        ),
    };
    let request_body = GetEvents::build_query(variables);
    let response = send_query::<GetEvents>(request_body).await
        .expect("Request failed");
    response.into()
}

async fn send_query<Q>(query: graphql_client::QueryBody<Q::Variables>) -> Result<Q::ResponseData, Box<dyn Error>>
    where
        Q: graphql_client::GraphQLQuery
{
    let client = surf::Client::new();

    let mut url = url::Url::parse("https://api.denkmal.org/graphql")
        .expect("Cannot parse URL");
    let variables = serde_json::to_string(&query.variables)
        .expect("Cannot serialize query variables");
    url.query_pairs_mut().append_pair("query", query.query);
    url.query_pairs_mut().append_pair("operationName", query.operation_name);
    url.query_pairs_mut().append_pair("variables", &variables);
    let request = client.get(url);

    let mut response = request.await
        .expect("Error sending HTTP request");

    if response.status().is_success() {
        let response: Response<Q::ResponseData> = response.body_json().await
            .expect("Cannot parse response");
        match response {
            Response { data: Some(data), .. } => {
                Ok(data)
            }
            Response { errors: Some(_errors), .. } => {
                Err("Received errors".into())
            }
            _ => {
                Err("Response without data nor errors".into())
            }
        }
    } else {
        let response_str = response.body_string().await
            .expect("Cannot decode response as string");
        Err(response_str.into())
    }
}
