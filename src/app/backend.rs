use std::pin::Pin;

use futures::FutureExt;
use futures::prelude::*;

use crate::actor::*;
use crate::app::*;
use crate::api::*;

pub struct BackendActor {}

impl BackendActor {
    pub fn new() -> Self {
        Self {}
    }
}

impl Actor for BackendActor {}


pub struct LoadExampleData { pub ui_addr: ActorAddr<UiActor> }

impl ActorReceive<LoadExampleData> for BackendActor {
    fn receive(&mut self, msg: LoadExampleData, _context: &mut ActorExecutionContext<Self>) -> Pin<Box<dyn Future<Output=()> + Send>> {
        receive_example(msg).boxed()
    }
}

async fn receive_example(msg: LoadExampleData) {
    let mut data = get_events().await;
    let mut region = data.regions.pop().unwrap();
    let events = region.days.pop().unwrap();
    msg.ui_addr.send_ok(SetEvents {
        events,
    });
}
