use std::error::Error;
use std::fmt;
use std::future::Future;
use std::marker::PhantomData;
use std::pin::Pin;

use futures::channel::mpsc;
use futures::StreamExt;
use futures::task::LocalSpawnExt;
use futures::task::SpawnExt;

/// An `Actor` can be spawned, and then receive messages.
pub trait Actor
    where
        Self: Sized
{
    /// Called when the actor is spawned/started.
    #[allow(unused_variables)]
    fn started(&mut self, context: &mut ActorExecutionContext<Self>) {}
}


/// Trait to implement on actors for receiving messages.

pub trait ActorReceive<M>
    where
        Self: Sized,
{
    fn receive(&mut self, msg: M, context: &mut ActorExecutionContext<Self>) -> Pin<Box<dyn Future<Output=()> + Send>>;
}


/// Context passed to `receive()` for accessing the actor's own address.

pub struct ActorExecutionContext<A> where {
    tx: ActorAddr<A>,
}

impl<A> ActorExecutionContext<A> where {
    pub fn addr(&self) -> ActorAddr<A> {
        self.tx.clone()
    }
}


/// The scheduler creates futures to process actor messages, and spawns them on a future spawner.

pub struct ActorScheduler {
    thread_pools: Vec<futures::executor::ThreadPool>,
}

impl ActorScheduler {
    pub fn new() -> Self {
        Self {
            thread_pools: vec!(),
        }
    }

    pub fn spawn_with<A, S>(&mut self, actor: A, spawner: &mut S) -> ActorAddr<A>
        where
            A: Actor + Send + 'static,
            S: futures::task::Spawn + 'static
    {
        let (future, addr) = self.create_future(actor);
        spawner.spawn(future).unwrap();
        addr
    }

    pub fn spawn_local_with<A, S>(&mut self, actor: A, spawner: &mut S) -> ActorAddr<A>
        where
            A: Actor + 'static,
            S: futures::task::LocalSpawn + 'static
    {
        let (future, addr) = self.create_local_future(actor);
        spawner.spawn_local(future).unwrap();
        addr
    }

    pub fn spawn_in_thread<A>(&mut self, actor: A) -> ActorAddr<A>
        where
            A: Actor + Send + 'static,
    {
        let mut pool_executor = futures::executor::ThreadPool::builder()
            .pool_size(1)
            .create()
            .expect("Cannot create future-ThreadPool");
        let addr = self.spawn_with(actor, &mut pool_executor);
        self.thread_pools.push(pool_executor);
        addr
    }

    fn create_stream<A>(&mut self) -> (impl futures::stream::Stream<Item=Envelope<A>>, ActorAddr<A>) {
        let (tx, rx) = actor_channel();
        let stream = rx.stream()
            .take_while(|envelope| {
                let terminate = match &envelope.payload {
                    EnvelopePayload::Terminate => true,
                    _ => false,
                };
                futures::future::ready(!terminate)
            });
        (stream, tx)
    }

    fn create_future<A>(&mut self, mut actor: A) -> (impl Future<Output=()> + Send, ActorAddr<A>)
        where A: Actor + Send + 'static
    {
        let (stream, tx) = self.create_stream();
        let mut context = ActorExecutionContext { tx: tx.clone() };
        actor.started(&mut context);
        let future = stream.for_each(move |envelope| {
            match envelope.payload {
                EnvelopePayload::Message(data) =>
                    futures::future::Either::Left(data.handle_with_actor(&mut actor, &mut context)),
                EnvelopePayload::Terminate =>
                    futures::future::Either::Right(futures::future::ready(())),
            }
        });
        (future, tx)
    }

    fn create_local_future<A>(&mut self, mut actor: A) -> (impl Future<Output=()>, ActorAddr<A>)
        where A: Actor + 'static
    {
        let (stream, tx) = self.create_stream();
        let mut context = ActorExecutionContext { tx: tx.clone() };
        actor.started(&mut context);
        let future = stream.for_each(move |envelope| {
            match envelope.payload {
                EnvelopePayload::Message(data) =>
                    futures::future::Either::Left(data.handle_with_actor(&mut actor, &mut context)),
                EnvelopePayload::Terminate =>
                    futures::future::Either::Right(futures::future::ready(())),
            }
        });
        (future, tx)
    }
}


/// Wrapper around an actor message, generic over the actor `A`.
/// Contains a boxed trait object, generic over the message type `M`.

struct Envelope<A> {
    pub payload: EnvelopePayload<A>,
}

enum EnvelopePayload<A> {
    Message(Box<dyn EnvelopeMessageTrait<Actor=A>>),
    Terminate,
}

impl<A> Envelope<A> {
    fn new_message<M>(msg: M) -> Self
        where
            M: Send + 'static,
            A: ActorReceive<M> + 'static
    {
        let payload = EnvelopePayload::Message(Box::new(EnvelopeMessage {
            msg,
            actor_phantom: PhantomData,
        }));
        Envelope { payload }
    }

    fn new_terminate() -> Self {
        let payload = EnvelopePayload::Terminate;
        Envelope { payload }
    }
}


struct EnvelopeMessage<A, M>
    where
        M: Send,
{
    actor_phantom: PhantomData<A>,
    msg: M,
}

// Unsafely marking `EnvelopeMessage` as `Send`, because the only non-`Send` field is phantom data.
unsafe impl<A, M> Send for EnvelopeMessage<A, M>
    where
        M: Send,
{}

trait EnvelopeMessageTrait: Send {
    type Actor;

    fn handle_with_actor(self: Box<Self>, actor: &mut Self::Actor, context: &mut ActorExecutionContext<Self::Actor>) -> Pin<Box<dyn Future<Output=()> + Send>>;
}

impl<A, M> EnvelopeMessageTrait for EnvelopeMessage<A, M>
    where
        A: ActorReceive<M>,
        M: Send,
{
    type Actor = A;

    fn handle_with_actor(self: Box<Self>, actor: &mut Self::Actor, context: &mut ActorExecutionContext<Self::Actor>) -> Pin<Box<dyn Future<Output=()> + Send>> {
        <Self::Actor as ActorReceive<M>>::receive(actor, self.msg, context)
    }
}


/// Channel for sending and receiving actor messages.

fn actor_channel<A>() -> ActorChannel<A> {
    let (tx, rx) = mpsc::unbounded();
    (ActorAddr { tx }, ActorRecv { rx })
}

type ActorChannel<A> = (
    ActorAddr<A>,
    ActorRecv<A>,
);

pub struct ActorAddr<A> {
    tx: mpsc::UnboundedSender<Envelope<A>>,
}

impl<A> ActorAddr<A> {
    pub fn send<M>(&self, message: M) -> Result<(), ActorSendError>
        where
            M: Send + 'static,
            A: ActorReceive<M> + 'static,
    {
        self.send_envelope(Envelope::new_message(message))
    }

    pub fn send_ok<M>(&self, message: M)
        where
            M: Send + 'static,
            A: ActorReceive<M> + 'static,
    {
        self.send(message).ok();
    }

    pub fn terminate(&self) {
        self.send_envelope(Envelope::new_terminate())
            .expect("Cannot send 'termination' message.");
    }

    pub fn clone(&self) -> Self {
        Self { tx: self.tx.clone() }
    }

    fn send_envelope(&self, envelope: Envelope<A>) -> Result<(), ActorSendError> {
        self.tx.unbounded_send(envelope)
            .map_err(|e| ActorSendError { source: e.into_send_error() })
    }
}

#[derive(Debug)]
pub struct ActorSendError {
    source: mpsc::SendError
}

impl fmt::Display for ActorSendError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Cannot send message to actor: {}", self.source)
    }
}

impl Error for ActorSendError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        Some(&self.source)
    }
}

pub struct ActorRecv<A> {
    rx: mpsc::UnboundedReceiver<Envelope<A>>,
}

impl<A> ActorRecv<A> {
    fn stream(self) -> impl futures::Stream<Item=Envelope<A>> {
        self.rx
    }
}
