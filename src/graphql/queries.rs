pub struct GetEvents;

pub mod get_events {
    #![allow(dead_code)]

    pub const OPERATION_NAME: &'static str = "GetEvents";
    pub const QUERY: &'static str = "query GetEvents($eventDays: [EventDay!]!) {\n    regions {\n        slug\n        events(eventDays: $eventDays) {\n            id\n            isPromoted\n            description\n            hasTime\n            from\n            until\n            eventDay\n            links {\n                label\n                url\n            }\n            genres {\n                name\n                category {\n                    color\n                }\n            }\n            tags\n            venue {\n                id\n                name\n                address\n                url\n                facebookPageId\n                latitude\n                longitude\n                region {\n                    slug\n                    dayOffset\n                    timeZone\n                }\n            }\n        }\n    }\n}\n";

    use serde::{Serialize, Deserialize};

    #[allow(dead_code)]
    type Boolean = bool;
    #[allow(dead_code)]
    type Float = f64;
    #[allow(dead_code)]
    type Int = i64;
    #[allow(dead_code)]
    type ID = String;
    #[doc = "ISO 8601 date string"]
    type Date = super::super::Date;
    #[doc = "Date with format: YYYY-MM-DD"]
    type EventDay = super::super::EventDay;
    #[doc = "URL string"]
    type URL = super::super::URL;
    #[doc = "An RFC 4122 compatible UUID String"]
    type UUID = super::super::UUID;

    #[derive(Clone, Debug, Deserialize, PartialEq)]
    pub struct GetEventsRegionsEventsLinks { pub label: String, pub url: URL }

    #[derive(Clone, Debug, Deserialize, PartialEq)]
    pub struct GetEventsRegionsEventsGenresCategory { pub color: Option<String> }

    #[derive(Clone, Debug, Deserialize, PartialEq)]
    pub struct GetEventsRegionsEventsGenres { pub name: String, pub category: GetEventsRegionsEventsGenresCategory }

    #[derive(Clone, Debug, Deserialize, PartialEq)]
    pub struct GetEventsRegionsEventsVenueRegion { pub slug: String, #[serde(rename = "dayOffset")] pub day_offset: Float, #[serde(rename = "timeZone")] pub time_zone: String }

    #[derive(Clone, Debug, Deserialize, PartialEq)]
    pub struct GetEventsRegionsEventsVenue { pub id: UUID, pub name: String, pub address: Option<String>, pub url: Option<String>, #[serde(rename = "facebookPageId")] pub facebook_page_id: Option<String>, pub latitude: Option<Float>, pub longitude: Option<Float>, pub region: GetEventsRegionsEventsVenueRegion }

    #[derive(Clone, Debug, Deserialize, PartialEq)]
    pub struct GetEventsRegionsEvents { pub id: UUID, #[serde(rename = "isPromoted")] pub is_promoted: Boolean, pub description: String, #[serde(rename = "hasTime")] pub has_time: Boolean, pub from: Date, pub until: Option<Date>, #[serde(rename = "eventDay")] pub event_day: EventDay, pub links: Vec<Option<GetEventsRegionsEventsLinks>>, pub genres: Vec<Option<GetEventsRegionsEventsGenres>>, pub tags: Vec<Option<String>>, pub venue: GetEventsRegionsEventsVenue }

    #[derive(Clone, Debug, Deserialize, PartialEq)]
    pub struct GetEventsRegions { pub slug: String, pub events: Option<Vec<Option<Vec<Option<GetEventsRegionsEvents>>>>> }

    #[derive(Clone, Debug, PartialEq, Serialize)]
    pub struct Variables { #[serde(rename = "eventDays")] pub event_days: Vec<EventDay> }

    impl Variables {}

    #[derive(Clone, Debug, Deserialize, PartialEq)]
    pub struct ResponseData { pub regions: Option<Vec<Option<GetEventsRegions>>> }
}

impl graphql_client::GraphQLQuery for GetEvents {
    type Variables = get_events::Variables;
    type ResponseData = get_events::ResponseData;
    fn build_query(variables: Self::Variables) -> ::graphql_client::QueryBody<Self::Variables> { graphql_client::QueryBody { variables, query: get_events::QUERY, operation_name: get_events::OPERATION_NAME } }
}
