pub use queries::*;

pub mod queries;

pub type Date = String;
pub type EventDay = String;
pub type URL = String;
pub type UUID = String;
