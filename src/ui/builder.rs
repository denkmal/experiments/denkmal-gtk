use gtk::prelude::*;

#[derive(Clone)]
pub struct Builder {
    gtk_builder: gtk::Builder,
}

impl Builder {
    pub fn register_resources(resources: Vec<&'static [u8]>) {
        for resource in resources {
            let glib_bytes = glib::Bytes::from_static(resource.as_ref());
            let gio_resource = gio::Resource::new_from_data(&glib_bytes)
                .expect("Cannot create gio::Resource from bytes.");
            gio::resources_register(&gio_resource);
        }
    }

    pub fn new() -> Self {
        let gtk_builder = gtk::Builder::new();
        Self {
            gtk_builder,
        }
    }

    pub fn load_ui(&self, resource_path: &str) {
        self.gtk_builder
            .add_from_resource(resource_path)
            .expect(&format!("Can't load ui file: {}", resource_path));
    }

    pub fn load_css(&self, resource_path: &str) {
        let provider = gtk::CssProvider::new();
        provider.load_from_resource(resource_path);
        let screen = gdk::Screen::get_default()
            .expect("Error initializing gdk::Screen.");
        gtk::StyleContext::add_provider_for_screen(&screen, &provider, gtk::STYLE_PROVIDER_PRIORITY_APPLICATION);
    }

    pub fn get_object<T: IsA<glib::Object>>(&self, name: &str) -> T {
        self.gtk_builder
            .get_object(name)
            .expect(&format!("Can't get UI object: {}", name))
    }
}
