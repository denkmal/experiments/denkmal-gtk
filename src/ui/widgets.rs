use gtk::{ContainerExt, LabelExt, ListBoxExt};

use crate::graphql::get_events::*;
use crate::ui::*;

pub struct Widgets {
    pub main_window: gtk::ApplicationWindow,
    pub event_list: gtk::ListBox,
    pub action_button: gtk::Button,
}

impl Widgets {
    pub fn new() -> Self {
        Builder::register_resources(vec!(
            include_bytes!("../../res/resources.gresource")
        ));
        let builder = Builder::new();
        builder.load_ui("/com/example/MyGtkWin/ui/main_window.ui");
        builder.load_ui("/com/example/MyGtkWin/ui/event_box.ui");
        builder.load_css("/com/example/MyGtkWin/app.css");

        Self {
            main_window: builder.get_object("app_window"),
            event_list: builder.get_object("event_list"),
            action_button: builder.get_object("action_button"),
        }
    }

    pub fn main_window(&self) -> &gtk::ApplicationWindow {
        &self.main_window
    }

    pub fn update_events(&self, events: Vec<GetEventsRegionsEvents>) {
        for child in self.event_list.get_children().iter() {
            self.event_list.remove(child);
        }
        for (i, event) in events.into_iter().enumerate() {
            let event_box = self.create_event_box(event.venue.name, event.description);
            self.event_list.insert(&event_box, i as i32);
        }
    }

    fn create_event_box(&self, venue: String, description: String) -> gtk::Box {
        let builder = Builder::new();
        builder.load_ui("/com/example/MyGtkWin/ui/event_box.ui");

        let label_venue: gtk::Label = builder.get_object("venue");
        label_venue.set_text(&venue);

        let label_description: gtk::Label = builder.get_object("description");
        label_description.set_markup(&format!("Description: <b>test</b>: {}", description));

        builder.get_object("event_box")
    }
}
